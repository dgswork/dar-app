# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
## Changed - 2020-08-27
- Scaffolding for pyramid and reactjs added
- Python structure for Kivy added (desktop_app)
## Added - 2020-08-26
- Scaffolding
